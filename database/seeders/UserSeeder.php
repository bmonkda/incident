<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'José Moncada',
            'email' => 'jose@correo.com',
            'password' => bcrypt('12345678')
          ])->assignRole('Admin');

          $user2 = User::create([
            'name' => 'Usuario Cualquiera',
            'email' => 'usuario@correo.com',
            'password' => bcrypt('12345678')
          ])->assignRole('Gestor');
          
          $user3 = User::create([
            'name' => 'Soporte Técnico',
            'email' => 'soporte@correo.com',
            'password' => bcrypt('12345678')
          ])->assignRole('Soporte');
          
      
          User::factory(7)->create();
    }
}
