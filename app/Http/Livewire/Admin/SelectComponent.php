<?php

namespace App\Http\Livewire\Admin;

use App\Models\Category;
use App\Models\Subcategory;
use Livewire\Component;

class SelectComponent extends Component
{
    public $category, $subcategory;
    public $categories = [], $subcategories = [];
    // public $category=null, $subcategory=null;
    // public $categories = null, $subcategories = null;

    public function mount()
    {
        $this->categories = Category::all();
        $this->subcategories = collect();
    }

    public function updatedCategory($valor)
    {
        $this->subcategories = Subcategory::where('category_id', $valor)->get();

        $this->subcategory = $this->subcategories->first()->id ?? null;

    }
    
    public function render()
    {
        return view('livewire.admin.select-component');
    }
}
