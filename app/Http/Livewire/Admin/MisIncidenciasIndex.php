<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

use App\Models\Incidencia;

use Livewire\WithPagination;

class MisIncidenciasIndex extends Component
{
    use WithPagination;

    public $search;
    
    protected $paginationTheme = 'bootstrap';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $incidencias = Incidencia::where('user_id', auth()->user()->id)
                        // ->where('name', 'LIKE', '%' . $this->search . '%')
                        ->where('titulo', 'LIKE', '%' . $this->search . '%')
                        ->latest('id')
                        ->paginate(10);
        return view('livewire.admin.mis-incidencias-index', compact('incidencias'));
    }
}
