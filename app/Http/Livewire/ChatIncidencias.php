<?php

namespace App\Http\Livewire;

use App\Models\Incidencia;
use App\Models\Message;
use App\Models\User;
use Livewire\Component;

class ChatIncidencias extends Component
{
    public $user;
    public $mensaje;
    public $incidencia;
    
    public function mount(Incidencia $incidencia, Message $messages)
    {
        $this->user = "";
        $this->messages = $messages;
        $this->incidencia = $incidencia;
    }

    public function render()
    {
        // $incidencia = Incidencia::where('id', '$incidencia->id')->get();
        // $incidencia = Incidencia::all();
        return view('livewire.chat-incidencias'/* , compact('incidencia') */);
    }

    /* esto es para probar mas adelante */
    public function enviarMensaje()
    {
        /* Message::create([
            'message' => $this->mensaje,
        ]); */

        // $this->emit("mensajeEnviado");

    }
}
