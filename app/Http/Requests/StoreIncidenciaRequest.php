<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreIncidenciaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // return false;
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'slug' => 'required|unique:incidencias,slug,',
            'descripcion' => 'required',
            // 'category_id' => 'required',
            'subcategory_id' => 'required',
            'emergency_id' => 'required',
            'estatu_id' => 'required',
        ];
        
        return $rules;
    }
}
