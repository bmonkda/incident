<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IncidenciaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return true;
        
        /* if ($this->user_id == auth()->user()->id) {
            return true;
        } else {
            return false;
        } */
        
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $incidencia = $this->route()->parameter('incidencia');

        $rules = [
            // 'name' => 'required',
            'titulo' => 'required',
            'slug' => 'required|unique:incidencias,slug,',
            'status' => 'required|in:1,2', // pendiente
            'file' => 'image',
        ];

        if ($incidencia) {
            $rules['slug'] = 'required|unique:incidencias,slug,' . $incidencia->id;
        }

        if ($this->status == 2) {
            $rules = array_merge($rules, [
                'category_id' => 'required',
                'modos' => 'required', // pendiente
                // 'extracto' => 'required',
                'descripcion' => 'required',
            ]);
        }

        return $rules;
    }
}
