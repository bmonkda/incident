<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IncidenciaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return true;
        
        /* if ($this->user_id == auth()->user()->id) {
            return true;
        } else {
            return false;
        } */
        
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $incidencia = $this->route()->parameter('incidencia');

        $rules = [
            'titulo' => 'required',
            'slug' => 'required|unique:incidencias,slug,',
            'descripcion' => 'required',
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'emergency_id' => 'required',
            // 'estatu_id' => 'required|in:1,2', // pendiente
            'estatu_id' => 'required', // pendiente
            'file' => 'image',
        ];

        if ($incidencia) {
            $rules['slug'] = 'required|unique:incidencias,slug,' . $incidencia->id;
        }

        // if ($this->status == 2) {
        //     $rules = array_merge($rules, [
        //         'category_id' => 'required',
        //         'modos' => 'required', // pendiente
        //         // 'extracto' => 'required',
        //         'descripcion' => 'required',
        //     ]);
        // }

        return $rules;
    }
}
