<?php

namespace App\Observers;

use App\Models\Incidencia;
use Illuminate\Support\Facades\Storage;

class IncidenciaObserver
{
    /**
     * Handle the Incidencia "created" event.
     *
     * @param  \App\Models\Incidencia  $incidencia
     * @return void
     */
    public function creating(Incidencia $incidencia)
    {
        if (! \App::runningInConsole()) {
            $incidencia->user_id = auth()->user()->id;
        }
    }

    /**
     * Handle the Incidencia "updated" event.
     *
     * @param  \App\Models\Incidencia  $incidencia
     * @return void
     */
    public function updated(Incidencia $incidencia)
    {
        //
    }

    /**
     * Handle the Incidencia "deleted" event.
     *
     * @param  \App\Models\Incidencia  $incidencia
     * @return void
     */
    public function deleting(Incidencia $incidencia)
    {
        if ($incidencia->image) {
            Storage::delete($incidencia->image->url);
        }
    }

    /**
     * Handle the Incidencia "restored" event.
     *
     * @param  \App\Models\Incidencia  $incidencia
     * @return void
     */
    public function restored(Incidencia $incidencia)
    {
        //
    }

    /**
     * Handle the Incidencia "force deleted" event.
     *
     * @param  \App\Models\Incidencia  $incidencia
     * @return void
     */
    public function forceDeleted(Incidencia $incidencia)
    {
        //
    }
}
