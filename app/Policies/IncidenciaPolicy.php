<?php

namespace App\Policies;

use App\Models\Incidencia;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class IncidenciaPolicy
{
    use HandlesAuthorization;

    public function author(User $user, Incidencia $incidencia){
        
        return $user->id === $incidencia->user_id;
    }

    public function published(?User $user, Incidencia $incidencia){
        if ($incidencia->status == 2) {
            return true;
        } else {
            return false;
        }
        
    }

}
