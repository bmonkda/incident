<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\IncidenciaController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\MisIncidenciaController;
use App\Http\Controllers\Admin\ModoController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\SubcategoryController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\EstatuController;
use App\Models\Estatu;
use Illuminate\Support\Facades\Route;

Route::get('', [HomeController::class, 'index'])->middleware('can:admin.home')->name('admin.home');

Route::resource('users', UserController::class)->only(['index', 'edit', 'update'])->names('admin.users');

Route::resource('roles', RoleController::class)->names('admin.roles');

Route::resource('categories', CategoryController::class)->except('show')->names('admin.categories');

Route::resource('subcategories', SubcategoryController::class)->except('show')->names('admin.subcategories');

// Route::resource('modos', ModoController::class)->except('show')->names('admin.modos');
Route::resource('estatus', EstatuController::class)->except('show')->names('admin.estatus');

Route::resource('incidencias', IncidenciaController::class)->except('show')->names('admin.incidencias');

Route::resource('mis-incidencias', MisIncidenciaController::class)->except('show')->names('admin.mis-incidencias');