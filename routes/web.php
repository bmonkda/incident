<?php

use App\Http\Controllers\IncidenciaController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\SuscripcionController;
use Illuminate\Support\Facades\Route;



// Route::get('/',[IncidenciaController::class, 'index'])->name('incidencias.index');
Route::get('/',[IncidenciaController::class, 'index'])->name('incidencias.index')->middleware('auth');

Route::get('incidencias/{incidencia}',[IncidenciaController::class, 'show'])->name('incidencias.show');

Route::get('category/{category}', [IncidenciaController::class, 'category'])->name('incidencias.category');

// Route::get('modo/{modo}', [IncidenciaController::class, 'modo'])->name('incidencias.modo');

// Route::resource('suscripciones', SuscripcionController::class)->names('suscripciones');


/* Mensaje */
Route::post('/mensajes',[MessageController::class, 'store'])->name('mensajes.store');


Route::get('markAsRead', function () {
    auth()->user()->unreadNotifications->markAsRead();
    return redirect()->back();
})->name('markAsRead');



Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
