@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Editar incidencias</h1>
@stop

@section('content')

    @if ( session('info') )
        <div class="alert alert-success">
            <strong>{{ session('info') }}</strong>
        </div>
    @endif

    <div class="card">
        <div class="card-body">
            {!! Form::model($incidencia, ['route' => ['admin.incidencias.update', $incidencia], 'autocomplete' => 'off', 'files' => true, 'method' => 'PUT']) !!}

                {!! Form::hidden('user_id', auth()->user()->id) !!}

                @include('admin.incidencias.partials.form')

                @if (auth()->user()->hasRole('Admin'))
                
                    <div class="form-group">
                        {!! Form::label('estatu_id', 'Estado*:') !!}
                        {!! Form::select('estatu_id', $estatus, null, ['class' => 'form-control', 'id' => 'estatu_id', 'placeholder' => 'Selecione una opción']) !!}
                    
                        @error('estatu_id')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    
                    <div class="form-group">
                        {!! Form::label('estatu_id', 'Asignado a:') !!}
                        {!! Form::select('estatu_id', $estatus, null, ['class' => 'form-control', 'id' => 'estatu_id', 'placeholder' => 'Selecione una opción']) !!}
                    
                        @error('estatu_id')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                
                @endif


                {!! Form::submit('Actualizar incidencia', ['class' => 'btn btn-primary mb-4']) !!}


                {{-- ----------------------------------------------------------------- --}}
                {{-- ----------------   aqui es para probar el chat   ---------------- --}}
                {{-- ----------------------------------------------------------------- --}}

                    @livewire('chat-incidencias', ['incidencia' => $incidencia/* , 'messages' => $messages */]/* , key($messages->id) */)
                    {{-- @livewire('component', ['user' => $user], key($user->id)) --}}

                {{-- ----------------------------------------------------------------- --}}
                {{-- ----------------      hasta aqui es el chat      ---------------- --}}
                {{-- ----------------------------------------------------------------- --}}
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('css')
    <style>
        .image-wrapper{
            position: relative;
            padding-bottom: 56.25%;
        }
        .image-wrapper img{
            position: absolute;
            object-fit: cover;
            width: 100%;
            height: 100%;
        }
    </style>
@stop

@section('js')
    <script src="{{ asset('vendor/jQuery-Plugin-stringToSlug-1.3/jquery.stringToSlug.min.js') }}"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/32.0.0/classic/ckeditor.js"></script>

    <script>
        $(document).ready( function() {
            $("#titulo").stringToSlug({
                setEvents: 'keyup keydown blur',
                getPut: '#slug',
                space: '-'
            });
        });

        // ClassicEditor
        // .create( document.querySelector( '#titulo' ) )
        // .catch( error => {
        //     console.error( error );
        // } );

        ClassicEditor
        .create( document.querySelector( '#descripcion' ) )
        .catch( error => {
            console.error( error );
        } );

        // Cambiar imagen
        document.getElementById("file").addEventListener('change', cambiarImagen);

        function cambiarImagen(e) {
            var file = e.target.files[0];
            var reader = new FileReader();
            reader.onload = (e) => {
                document.getElementById("picture").setAttribute('src', e.target.result);
            };
            reader.readAsDataURL(file);
        }
        
    </script>
@endsection