<x-app-layout>
    {{-- <div class="container py-8">
        <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3  gap-6">
            @foreach ($incidencias as $incidencia)
                <article class="w-full h-80 bg-cover bg-center @if($loop->first) md:col-span-2 @endif" style="background-image:url( @if($incidencia->image) {{ Storage::url($incidencia->image->url) }} @else https://cdn.pixabay.com/photo/2021/08/04/13/06/software-developer-6521720_960_720.jpg @endif )">
                    <div class="w-full h-full px-8 flex flex-col justify-center">

                        <div>

                                <a href="#" class="inline-block px-3 h-6 bg-{{ $incidencia->estatu->color }} text-white rounded-full">{{ $incidencia->estatu->name }}</a>

                        </div>

                        <h1 class="text-4xl text-white leading-8 font-bold mt-2">
                            <a href={{ route('incidencias.show', $incidencia) }}>
                                {{ $incidencia->titulo }}
                            </a>
                        </h1>
                    </div>

                </article>
            @endforeach
        </div>

        <div class="mt-4">
            {{ $incidencias->links() }}
        </div>

    </div> --}}

    <div class="container py-8">
        

        <a class="btn btn-success btn-sm float-right" href="#">Nueva incidencia</a>
        <h1 class="uppercase text-center text-3xl font-bold">Lista de Incidencias</h1>
        <div class="card">
            <div class="card-body">
            
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Usuario</th>
                            <th>Categoría</th>
                            <th>Prioridad</th>
                            <th>Estatu</th>
                            <th colspan="2"></th>
                        </tr>
                    </thead>
    
                    <tbody>
                        @foreach ($incidencias as $incidencia)
                            <tr>
                                <td>{{ $incidencia->id }}</td>
                                <td>{{ $incidencia->user->name }}</td>
                                <td>{{ $incidencia->subcategory->category->name }}</td>
                                <td>{{ $incidencia->emergency->name }}</td>
                                <td>{{ $incidencia->estatu->name }}</td>
    
                                <td width="10px">
                                    {{-- <a href="{{ route('incidencias.edit', $incidencia) }}" class="btn btn-primary bt-sm">Editar</a> --}}
                                    <a href="#" class="btn btn-primary bt-sm">Editar</a>
                                </td>
                                <td width="10px">
                                    {{-- <a href="{{ route('incidencias.edit', $incidencia) }}" class="btn btn-primary bt-sm">Editar</a> --}}
                                    <a href="#" class="btn btn-primary bt-sm">Ver</a>
                                </td>

                                <td width="10px">
                                    {{-- <form action="{{ route('', $incidencia) }}" method="POST"> --}}
                                    <form action="#" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger bt-sm">Eliminar</button>
                                    </form>
                                </td>
                            </tr>

                        @endforeach
                    </tbody>
                </table>
                {{$incidencias->links()}}
            
            </div>
            
        </div>
    </div>

</x-app-layout>