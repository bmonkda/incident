<x-app-layout>
    <div class="container py-8">
        <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3  gap-6">
            @foreach ($incidencias as $incidencia)
                <article class="w-full h-80 bg-cover bg-center @if($loop->first) md:col-span-2 @endif" style="background-image:url( @if($incidencia->image) {{ Storage::url($incidencia->image->url) }} @else https://cdn.pixabay.com/photo/2021/08/04/13/06/software-developer-6521720_960_720.jpg @endif )">
                    <div class="w-full h-full px-8 flex flex-col justify-center">

                        <div>

                                <a href="#" class="inline-block px-3 h-6 bg-{{ $incidencia->estatu->color }} text-white rounded-full">{{ $incidencia->estatu->name }}</a>

                        </div>

                        <h1 class="text-4xl text-white leading-8 font-bold mt-2">
                            <a href={{ route('incidencias.show', $incidencia) }}>
                                {{ $incidencia->titulo }}
                            </a>
                        </h1>
                    </div>

                </article>
            @endforeach
        </div>

        <div class="mt-4">
            {{ $incidencias->links() }}
        </div>

    </div>

</x-app-layout>