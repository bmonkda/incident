@props(['incidencia'])

<article class="mb-8 bg-white shadow-lg rounded-lg overflow-hidden">
    @if ($incidencia->image)
        <img class="w-full h-72 object-cover object-center" src="{{ Storage::url($incidencia->image->url) }}" alt="">
    @else
        <img class="w-full h-72 object-cover object-center" src="https://cdn.pixabay.com/photo/2021/08/04/13/06/software-developer-6521720_960_720.jpg" alt="">
    @endif

    <div class="px-6 py-4">
        <h1 class="font-bold text-xl mb-2">
            {{-- <a href="{{ route('incidencias.show',$incidencia) }}">{{ $incidencia->name }}</a> --}}
            <a href="{{ route('incidencias.show',$incidencia) }}">{{ $incidencia->titulo }}</a>
        </h1>
        <div class="text-gray-700 text-base">
            {!! $incidencia->descripcion !!}
        </div>
    </div>

    <div class="px-6 pt-4 pb-2">
        {{-- @foreach ($incidencia->modos as $modo) --}}
        {{-- @foreach ($incidencias as $incidencia) --}}
            {{-- <a href="{{ route('incidencias.modo',$modo) }}" class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm text-gray-700 mr-2">{{ $modo->name }}</a> --}}
            {{-- <a href="{{ route('incidencias.modo',$modo) }}" class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm text-gray-700 mr-2">{{ $incidencia->titulo }}</a> --}}
            {{-- <a href="#" class="inline-block px-3 h-6 bg-{{ $incidencia->estatu->color }} text-white rounded-full">{{ $incidencia->estatu->name }}</a> --}}
            <a href="#" class="inline-block px-3 h-6 bg-{{ $incidencia->estatu->color }} text-white rounded-full">{{ $incidencia->estatu->name }}</a>
        {{-- @endforeach --}}
    </div>
    
</article>