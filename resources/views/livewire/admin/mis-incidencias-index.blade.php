<div class="card">
    <div class="card-header">
        <input wire:model="search" class="form-control" placeholder="Ingrese el nombre de incidencia">
    </div>
    
    @if ($incidencias->count())

        <div class="card-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        {{-- <th>Usuario</th> --}}
                        <th>Título</th>
                        <th>Categoría</th>
                        <th>Prioridad</th>
                        <th>Estatu</th>
                        <th colspan="2"></th>
                    </tr>
                </thead>

                <tbody>
                    
                    @foreach ($incidencias as $incidencia)
                        <tr>
                            <td>{{ $incidencia->id }}</td>
                            {{-- <td>{{ $incidencia->user->name }}</td> --}}
                            <td>{{ $incidencia->titulo }}</td>
                            <td>{{ $incidencia->subcategory->category->name }}</td>
                            <td>{{ $incidencia->emergency->name }}</td>
                            {{-- <td>{{ $incidencia->estatu->name }}</td> --}}
                            <td class="project-state"><span class="badge badge-{{ $incidencia->estatu->color }}">{{ $incidencia->estatu->name }}</span></td>
                            <td width="10px">
                                <a class="btn btn-primary btn-sm" href="{{ route('admin.incidencias.edit', $incidencia) }}" title="Editar">
                                    <i class="fas fa-pencil-alt" >
                                    </i>
                                    {{-- Editar --}}
                                </a>
                            </td>
                            <td width="10px">
                                <form action="{{ route('admin.incidencias.destroy', $incidencia) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-sm" title="Eliminar">
                                        <i class="fas fa-trash">
                                        </i>
                                        {{-- Eliminar --}}
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="card-footer">
            {{ $incidencias->links() }}
        </div>
    @else
        <div class="card-body">
            <strong>No hay ningún registro...</strong>
        </div>
    @endif

</div>
