<div>

    <div class="form-group">
        <label>Categoría*</label>
        {{-- <select name="" id=""></select> --}}
        <select class="form-control" wire:model="category" name="category_id">
            {{-- <option selected>Seleccione una categoría</option> --}}
            <option value="">Seleccione una categoría</option>
            @foreach ($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
        </select>

        @error('category_id')
            <small class="text-danger">{{ $message }}</small>
        @enderror
    </div>
    
    
    <div class="form-group">
        <label>Subcategoría*</label>
        <select class="form-control" wire:model="subcategory" name="subcategory_id">
            @if ($subcategories->count() == 0)
                {{-- <option selected>No hay subcategorías</option> --}}
                <option value="">Debe seleccionar una Categoría</option>
            @endif
            
            @foreach ($subcategories as $subcategory)
                <option value="{{ $subcategory->id }}">{{ $subcategory->name }}</option>
            @endforeach
            
        </select>

        @error('subcategory_id')
            <small class="text-danger">{{ $message }}</small>
        @enderror
    </div>

</div>
