<div>

    {{-- <div class="card card-danger direct-chat direct-chat-danger"> --}}
    {{-- <div class="card direct-chat direct-chat-primary"> --}}
    <div class="card card-secondary direct-chat-primary">

        <div class="card-header">
            <h3 class="card-title">Chat</h3>

            {{-- esto para después --}}
            {{-- <div class="card-tools">
                <span data-toggle="tooltip" title="3 New Messages" class="badge badge-light">3</span>
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle">
                <i class="fas fa-comments"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                </button>
            </div> --}}

        </div>
        <!-- /.card-header -->

        <div class="card-body">

            <!-- Conversations are loaded here -->
            <div class="direct-chat-messages">

                {{-- {{ $messages->get() }} --}}
                <!-- Message. Default to the left -->
                @foreach ($messages->get() as $message)

                    <div class="direct-chat-msg">
                        <div class="direct-chat-infos clearfix">
                            <span class="direct-chat-name float-left">{{ $message->user->name }}</span>
                            <span class="direct-chat-timestamp float-right">{{ $message->created_at }}</span>
                        </div>
                        <!-- /.direct-chat-infos -->
                        
                        {{-- <img class="direct-chat-img" src="/docs/3.2/assets/img/user1-128x128.jpg" alt="message user image"> --}}
                        {{-- <img class="direct-chat-img" src="/docs/3.2/assets/img/user1-128x128.jpg" alt=""> --}}
                        <img class="direct-chat-img" src="{{ $message->user->profile_photo_url }}" alt="">

                        <!-- /.direct-chat-img -->
                        <div class="direct-chat-text">
                            {{-- aqui va el mensaje --}}
                            {{ $message->message }} 
                        </div>
                        <!-- /.direct-chat-text -->
                    </div>
                    <!-- /.direct-chat-msg -->
                
                @endforeach

                <!-- Message to the right -->
                {{-- <div class="direct-chat-msg right">
                    <div class="direct-chat-infos clearfix">
                        <span class="direct-chat-name float-right">Sarah Bullock</span>
                        <span class="direct-chat-timestamp float-left">23 Jan 2:05 pm</span>
                    </div>
                    <!-- /.direct-chat-infos -->
                    <img class="direct-chat-img" src="/docs/3.2/assets/img/user3-128x128.jpg" alt="message user image">
                    <!-- /.direct-chat-img -->
                    <div class="direct-chat-text">
                        You better believe it!
                    </div>
                    <!-- /.direct-chat-text -->
                </div> --}}
                <!-- /.direct-chat-msg -->
                
            </div>
            <!--/.direct-chat-messages-->

            <!-- Contacts are loaded here -->
            {{-- <div class="direct-chat-contacts">
                <ul class="contacts-list">
                    <li>
                        <a href="#">
                        <img class="contacts-list-img" src="/docs/3.2/assets/img/user1-128x128.jpg">
                        <div class="contacts-list-info">
                            <span class="contacts-list-name">
                            Count Dracula
                            <small class="contacts-list-date float-right">2/28/2015</small>
                            </span>
                            <span class="contacts-list-msg">How have you been? I was...</span>
                        </div>
                        <!-- /.contacts-list-info -->
                        </a>
                    </li>
                    <!-- End Contact Item -->
                    <li>
                        <a href="#">
                        <img class="contacts-list-img" src="/docs/3.2/assets/img/user7-128x128.jpg">
                        <div class="contacts-list-info">
                            <span class="contacts-list-name">
                            Sarah Doe
                            <small class="contacts-list-date float-right">2/23/2015</small>
                            </span>
                            <span class="contacts-list-msg">I will be waiting for...</span>
                        </div>
                        <!-- /.contacts-list-info -->
                        </a>
                    </li>
                    <!-- End Contact Item -->
                    <li>
                        <a href="#">
                        <img class="contacts-list-img" src="/docs/3.2/assets/img/user3-128x128.jpg">
                        <div class="contacts-list-info">
                            <span class="contacts-list-name">
                            Nadia Jolie
                            <small class="contacts-list-date float-right">2/20/2015</small>
                            </span>
                            <span class="contacts-list-msg">I'll call you back at...</span>
                        </div>
                        <!-- /.contacts-list-info -->
                        </a>
                    </li>
                    <!-- End Contact Item -->
                    <li>
                        <a href="#">
                        <img class="contacts-list-img" src="/docs/3.2/assets/img/user5-128x128.jpg">
                        <div class="contacts-list-info">
                            <span class="contacts-list-name">
                            Nora S. Vans
                            <small class="contacts-list-date float-right">2/10/2015</small>
                            </span>
                            <span class="contacts-list-msg">Where is your new...</span>
                        </div>
                        <!-- /.contacts-list-info -->
                        </a>
                    </li>
                    <!-- End Contact Item -->
                    <li>
                        <a href="#">
                        <img class="contacts-list-img" src="/docs/3.2/assets/img/user6-128x128.jpg">
                        <div class="contacts-list-info">
                            <span class="contacts-list-name">
                            John K.
                            <small class="contacts-list-date float-right">1/27/2015</small>
                            </span>
                            <span class="contacts-list-msg">Can I take a look at...</span>
                        </div>
                        <!-- /.contacts-list-info -->
                        </a>
                    </li>
                    <!-- End Contact Item -->
                    <li>
                        <a href="#">
                        <img class="contacts-list-img" src="/docs/3.2/assets/img/user8-128x128.jpg">
                        <div class="contacts-list-info">
                            <span class="contacts-list-name">
                            Kenneth M.
                            <small class="contacts-list-date float-right">1/4/2015</small>
                            </span>
                            <span class="contacts-list-msg">Never mind I found...</span>
                        </div>
                        <!-- /.contacts-list-info -->
                        </a>
                    </li>
                    <!-- End Contact Item -->
                </ul>
                <!-- /.contacts-list -->
            </div> --}}
            <!-- /.direct-chat-pane -->

        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <form action="{{route('mensajes.store')}}" method="post">
                @csrf
                <div class="input-group">
                    <input type="hidden" name="incidencia_id" value= "{{ $incidencia->id }}" >
                    <input type="hidden" name="user_id" value= "{{ Auth::user()->id }}" >
                    <input type="text" name="message" id="message" wire:model="mensaje" placeholder="Escriba su mensaje..." class="form-control">
                    <span class="input-group-append">
                        <button type="submit" class="btn btn-primary" wire:click='enviarMensaje'>Enviar Mensaje</button>
                    </span>
                </div>
                <small>{{ $mensaje }}</small>
                {{-- @error('menssage')
                    <small class="text-danger">{{ $message }}</small>
                @enderror --}}
            </form>
        </div>
        <!-- /.card-footer-->

    </div>
    <!--/.direct-chat -->
    {{-- Esquema de chat directo --}}

</div>
